var svg = d3.select("#linkage")
    .append("svg")
    .attr("width",800)
    .attr("height",800);

var x = d3.scaleLinear()
    .domain([-2,2])
    .range([0,800]);
var y = d3.scaleLinear()
    .domain([-2,2])
    .range([800,0]);

var ul = x(1)-x(0);

var v0 = svg.append("circle")
    .attr("cx",x(-1))
    .attr("cy",y(0))
    .attr("r",0.05*ul);

var v1 = svg.append("circle")
    .attr("cx",x(1))
    .attr("cy",y(0))
    .attr("r",0.05*ul);

var v2 = svg.append("circle")
    .attr("cx",x(Math.cos(Math.PI*2/3)))
    .attr("cy",y(Math.sin(Math.PI*2/3)))
    .attr("r",0.05*ul)
    .call(d3.drag()
	  .on("start",dragstarted)
	  .on("drag",dragged)
	 );

var v3 = svg.append("circle")
    .attr("cx",x(0))
    .attr("cy",y(0))
    .attr("r",0.05*ul)
    .call(d3.drag()
	  .on("start",dragstarted3)
	  .on("drag",dragged3)
	 );

var l1 = svg.append("line")
    .attr("x1",x(-1))
    .attr("y1",y(0))
    .attr("x2",x(Math.cos(Math.PI*2/3)))
    .attr("y2",y(Math.sin(Math.PI*2/3)))
    .style("stroke","black");

var l2 = svg.append("line")
    .attr("x1",x(Math.cos(Math.PI*2/3)))
    .attr("y1",y(Math.sin(Math.PI*2/3)))
    .attr("x2",x(0))
    .attr("y2",y(0))
    .style("stroke","black");

var l3 = svg.append("line")
    .attr("x1",x(0))
    .attr("y1",y(0))
    .attr("x2",x(1))
    .attr("y2",y(0))
    .style("stroke","black");

var sgn3 = +1;
var sgn2 = -1;

function dist(A,B){
    return Math.sqrt((A[0]-B[0])*(A[0]-B[0]) + (A[1]-B[1])*(A[1]-B[1]));
}

function dragstarted(d){
    //ピクセル座標系からモデル座標系に逆変換
    var a = x.invert(d3.event.x);
    var b = y.invert(d3.event.y);
    // モデル座標系での計算
    var cx = -1 + (a+1)/Math.sqrt((a+1)*(a+1)+b*b);
    var cy = b/Math.sqrt((a+1)*(a+1)+b*b);
    var theta = Math.acos(Math.sqrt((cx-1)*(cx-1)+cy*cy)/2);
    var tau = Math.acos((1-cx)/Math.sqrt((cx-1)*(cx-1)+cy*cy));
    var dx = x.invert(v3.attr("cx"))
    var dy = y.invert(v3.attr("cy"))
    if (cy > 0){
	if (dist([1-Math.cos(tau-theta),Math.sin(tau-theta)],[dx,dy]) <
	    dist([1-Math.cos(tau+theta),Math.sin(tau+theta)],[dx,dy])){
	    sgn3 = -1;
	}else{
	    sgn3 = +1;
	}
    }else{
	if (dist([1-Math.cos(-tau-theta),Math.sin(-tau-theta)],[dx,dy]) <
	    dist([1-Math.cos(-tau+theta),Math.sin(-tau+theta)],[dx,dy])){
	    sgn3 = -1;
	}else{
	    sgn3 = +1;
	}
    }
    console.log(sgn3);
}

function dragged(d){
    //ピクセル座標系からモデル座標系に逆変換
    var a = x.invert(d3.event.x);
    var b = y.invert(d3.event.y);
    // モデル座標系での計算
    var cx = -1 + (a+1)/Math.sqrt((a+1)*(a+1)+b*b);
    var cy = b/Math.sqrt((a+1)*(a+1)+b*b);
    var theta = Math.acos(Math.sqrt((cx-1)*(cx-1)+cy*cy)/2);
    var tau = Math.acos((1-cx)/Math.sqrt((cx-1)*(cx-1)+cy*cy));
    console.log([theta,tau]);
    if (cy > 0){
	var dx = 1-Math.cos(sgn3*theta+tau);
	var dy = Math.sin(sgn3*theta+tau);
    }else{
	var dx = 1-Math.cos(sgn3*theta-tau);
	var dy = Math.sin(sgn3*theta-tau);
    }

    // ピクセル座標系に変換
    v2.attr("cx",x(cx)).attr("cy",y(cy));
    v3.attr("cx",x(dx)).attr("cy",y(dy));
    l1.attr("x2",x(cx)).attr("y2",y(cy));
    l2.attr("x1",x(cx)).attr("y1",y(cy)).attr("x2",x(dx)).attr("y2",y(dy));
    l3.attr("x1",x(dx)).attr("y1",y(dy));
    svg.append("circle")
	.attr("cx",x((cx+dx)/2))
	.attr("cy",y((cy+dy)/2))
	.attr("r",3)
	.style("fill","blue")
	.style("fill-opacity",0.2);
}


function dragstarted3(d){
    //ピクセル座標系からモデル座標系に逆変換
    var a = x.invert(d3.event.x);
    var b = y.invert(d3.event.y);
    // モデル座標系での計算
    var dx = 1 + (a-1)/Math.sqrt((a-1)*(a-1)+b*b);
    var dy = b/Math.sqrt((a-1)*(a-1)+b*b);
    var theta = Math.acos(Math.sqrt((dx+1)*(dx+1)+dy*dy)/2);
    var tau = Math.acos((1+dx)/Math.sqrt((1+dx)*(1+dx)+dy*dy));
    var cx = x.invert(v2.attr("cx"))
    var cy = y.invert(v2.attr("cy"))
    if (dy > 0){
	if (dist([-1+Math.cos(tau-theta),Math.sin(tau-theta)],[cx,cy]) <
	    dist([-1+Math.cos(tau+theta),Math.sin(tau+theta)],[cx,cy])){
	    sgn2 = -1;
	}else{
	    sgn2 = +1;
	}
    }else{
	if (dist([-1+Math.cos(-tau-theta),Math.sin(-tau-theta)],[cx,cy]) <
	    dist([-1+Math.cos(-tau+theta),Math.sin(-tau+theta)],[cx,cy])){
	    sgn2 = -1;
	}else{
	    sgn2 = +1;
	}
    }
    console.log(sgn2);
}

function dragged3(d){
    //ピクセル座標系からモデル座標系に逆変換
    var a = x.invert(d3.event.x);
    var b = y.invert(d3.event.y);
    // モデル座標系での計算
    var dx = 1 + (a-1)/Math.sqrt((a-1)*(a-1)+b*b);
    var dy = b/Math.sqrt((a-1)*(a-1)+b*b);


    var theta = Math.acos(Math.sqrt((dx+1)*(dx+1)+dy*dy)/2);
    var tau = Math.acos((1+dx)/Math.sqrt((dx+1)*(dx+1)+dy*dy));
    console.log([theta,tau]);
    if (dy > 0){
	var cx = -1+Math.cos(sgn2*theta+tau);
	var cy = Math.sin(sgn2*theta+tau);
    }else{
	var cx = -1+Math.cos(sgn2*theta-tau);
	var cy = Math.sin(sgn2*theta-tau);
    }

    
    // ピクセル座標系に変換
    v2.attr("cx",x(cx)).attr("cy",y(cy));
    v3.attr("cx",x(dx)).attr("cy",y(dy));
    l1.attr("x2",x(cx)).attr("y2",y(cy));
    l2.attr("x1",x(cx)).attr("y1",y(cy)).attr("x2",x(dx)).attr("y2",y(dy));
    l3.attr("x1",x(dx)).attr("y1",y(dy));
    svg.append("circle")
	.attr("cx",x((cx+dx)/2))
	.attr("cy",y((cy+dy)/2))
	.attr("r",3)
	.style("fill","blue")
	.style("fill-opacity",0.2);
}
